package com.zxhtom.book.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;

import com.zxhtom.book.config.model.BookConfig;
import com.zxhtom.book.model.Clazz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.book.model.Book;
import com.zxhtom.book.repository.BookRepository;
import com.zxhtom.book.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;
    @Autowired
    private BookConfig bookConfig;
	@Override
	public PagedResult<Book> selectBooksByPK(Long bookId,Integer pageNumber, Integer pageSize) {
		PagedResult<Book> result = new PagedResult<>();
		if(0==bookId){
			//查询所有
			bookId=null;
		}
		result = bookRepository.selectBooksByPK(bookId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteBookPK(Long bookId) {
		if(0==bookId){
			//删除所有
			bookId=null;
		}
		return bookRepository.deleteBooksByPK(bookId);
	}

	@Override
	public Integer updateBook(Book book) {
		return bookRepository.updateBook(book);
	}

	@Override
	public Integer insertBook(Book book) {
		book.setBookId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		return bookRepository.insertBook(book);
	}
	
	@Override
	public Integer deleteBookPKBatch(List<Long> bookIds){
		for(Long bookId : bookIds){
			bookRepository.deleteBooksByPK(bookId);
		}
		return 1;
	}
	
	@Override
	public Integer updateBookBatch(List<Book> books) {
		return bookRepository.updateBookBatch(books);
	}

	@Override
	public Integer insertBookBatch(List<Book> books) {
		for(Book book : books){
		book.setBookId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		}
		return bookRepository.insertBookBatch(books);
	}

    @Override
    public List<Clazz> selectBooksList() {
        List<Clazz> clazzes = new ArrayList<>();
        Clazz clazz = new Clazz();
        clazz.setClassName("全部");
        clazz.setBookList(bookRepository.selectAll());
        clazzes.add(clazz);
        clazzes.addAll(bookRepository.selectBooks());
        for (Clazz clazz1 : clazzes) {
            List<Book> bookList = clazz1.getBookList();
            for (Book book : bookList) {
                book.setBookUrl(bookConfig.getNginx()+book.getBookUrl());
            }
        }
        return clazzes;
    }

}
