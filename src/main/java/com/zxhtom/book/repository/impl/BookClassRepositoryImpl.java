package com.zxhtom.book.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.book.mapper.BookClassMapper;
import com.zxhtom.book.model.BookClass;
import com.zxhtom.book.repository.BookClassRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class BookClassRepositoryImpl implements BookClassRepository{

	@Autowired
	private BookClassMapper bookClassMapper;
	@Override
	public PagedResult<BookClass> selectBookClasssByPK(Long bookClassId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(BookClass.class,false,false);
		example.createCriteria().andEqualTo("bookClassId", bookClassId);
		return new PagedResult<>(bookClassMapper.selectByExample(example));
	}
	@Override
	public Integer deleteBookClasssByPK(Long bookClassId) {
		Example example = new Example(BookClass.class,false,false);
		example.createCriteria().andEqualTo("bookClassId", bookClassId);
		return bookClassMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateBookClass(BookClass bookClass) {
		return bookClassMapper.updateByPrimaryKeySelective(bookClass);
	}
	@Override
	public Integer insertBookClass(BookClass bookClass) {
		return bookClassMapper.insert(bookClass);
	}
	
	@Override
	public Integer updateBookClassBatch(List<BookClass> bookClasss) {
		return bookClassMapper.updateBookClassBatch(bookClasss);
	}
	@Override
	public Integer insertBookClassBatch(List<BookClass> bookClasss) {
		return bookClassMapper.insertBookClassBatch(bookClasss);
	}

	@Override
	public List<BookClass> selectAll() {
		return bookClassMapper.selectAll();
	}
}
