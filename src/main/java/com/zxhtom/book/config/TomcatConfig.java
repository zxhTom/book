package com.zxhtom.book.config;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class TomcatConfig {
    @Value("${server.port:8080}")
    private int port;
}

