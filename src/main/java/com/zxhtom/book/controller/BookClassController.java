package com.zxhtom.book.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.book.model.BookClass;
import com.zxhtom.book.service.BookClassService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/book/bookClass")
@Api(tags = "book bookClass", description = "书籍类别信息表管理")
public class BookClassController {

	private Logger logger = LogManager.getLogger(BookClassController.class);
	@Autowired
	private BookClassService bookClassService;


	@RequestMapping(value = "/selectAll", method = RequestMethod.GET)
	@ApiOperation(value = "获取所有类别")
	public List<BookClass> selectAll() {
		return bookClassService.selectALL();
	}

	@RequestMapping(value = "/{bookClassId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有书籍类别信息列表 传0表示查询所有")
	public PagedResult<BookClass> selectBookClasssByPK(@ApiParam(value = "书籍类别主键", required = true) @PathVariable @NotNull Long bookClassId,
	@ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
	@ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return bookClassService.selectBookClasssByPK(bookClassId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteBookClassByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long bookClassId) {
		return bookClassService.deleteBookClassPK(bookClassId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) BookClass bookClass) {
		return bookClassService.updateBookClass(bookClass);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody BookClass bookClass) {
		return bookClassService.insertBookClass(bookClass);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteBookClassByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> bookClassIds) {
		return bookClassService.deleteBookClassPKBatch(bookClassIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<BookClass> bookClasss) {
		return bookClassService.updateBookClassBatch(bookClasss);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<BookClass> bookClasss) {
		return bookClassService.insertBookClassBatch(bookClasss);
	}
}
