var clazzList=[{'name':'demo'}];
var vm = new Vue({
    el:'#book',
    data:{
        clazzList: clazzList
    },
    created:function(){
        var temp = null;
        $.ajax({
            type: "GET",
            url: "/book/book/clazzList",
            async: false,
            success: function(r){
                if(r.code === 0){
                    temp=r.data;
                }else{
                    alert(r.msg);
                }
            }
        });
        this.clazzList = temp;
    },
    methods: {
        showPdf:function(isShow) {
            var state = "";
            if (isShow) {
                state = "block";
            } else {
                state = "none";
            }
            var pop = document.getElementById("pop");
            pop.style.display = state;
            var lightbox = document.getElementById("lightbox");
            lightbox.style.display = state;
        },
        close:function() {
            this.showPdf(false);
        }
    }
});