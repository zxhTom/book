package com.zxhtom.book.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxhtom.utils.IdUtil;
import com.zxhtom.PagedResult;
import com.zxhtom.book.model.BookClass;
import com.zxhtom.book.repository.BookClassRepository;
import com.zxhtom.book.service.BookClassService;

@Service
public class BookClassServiceImpl implements BookClassService {

	@Autowired
	private BookClassRepository bookClassRepository;

	@Override
	public PagedResult<BookClass> selectBookClasssByPK(Long bookClassId,Integer pageNumber, Integer pageSize) {
		PagedResult<BookClass> result = new PagedResult<>();
		if(0==bookClassId){
			//查询所有
			bookClassId=null;
		}
		result = bookClassRepository.selectBookClasssByPK(bookClassId, pageNumber, pageSize);
		return result;
	}

	@Override
	public Integer deleteBookClassPK(Long bookClassId) {
		if(0==bookClassId){
			//删除所有
			bookClassId=null;
		}
		return bookClassRepository.deleteBookClasssByPK(bookClassId);
	}

	@Override
	public Integer updateBookClass(BookClass bookClass) {
		return bookClassRepository.updateBookClass(bookClass);
	}

	@Override
	public Integer insertBookClass(BookClass bookClass) {
		bookClass.setBookClassId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		return bookClassRepository.insertBookClass(bookClass);
	}
	
	@Override
	public Integer deleteBookClassPKBatch(List<Long> bookClassIds){
		for(Long bookClassId : bookClassIds){
			bookClassRepository.deleteBookClasssByPK(bookClassId);
		}
		return 1;
	}
	
	@Override
	public Integer updateBookClassBatch(List<BookClass> bookClasss) {
		return bookClassRepository.updateBookClassBatch(bookClasss);
	}

	@Override
	public Integer insertBookClassBatch(List<BookClass> bookClasss) {
		for(BookClass bookClass : bookClasss){
		bookClass.setBookClassId(BigDecimal.valueOf(IdUtil.getInstance().getId()));
		}
		return bookClassRepository.insertBookClassBatch(bookClasss);
	}

    @Override
    public List<BookClass> selectALL() {
        return bookClassRepository.selectAll();
    }

}
