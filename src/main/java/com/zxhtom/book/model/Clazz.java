package com.zxhtom.book.model;

import java.util.List;

public class Clazz {
    private String className;
    private List<Book> bookList;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }
}
