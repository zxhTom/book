package com.zxhtom.book;

import com.zxhtom.config.MybatisConfig;
import com.zxhtom.config.QuickStartConfig;
import com.zxhtom.config.SpringfoxConfig;
import com.zxhtom.config.webservice.WebserviceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * 入口类, 扫描并注入其他配置类和服务
 * @author John
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.zxhtom"})
@Import({QuickStartConfig.class, MybatisConfig.class,SpringfoxConfig.class})
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }


}
