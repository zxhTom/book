package com.zxhtom.book.mapper;


import java.util.List;

import com.zxhtom.book.model.Book;

import com.zxhtom.book.model.Clazz;
import tk.mybatis.mapper.common.Mapper;
public interface BookMapper extends Mapper<Book>{

	/**
	 * 批量更新
	 * @param books
	 * @return
	 */
	Integer updateBookBatch(List<Book> books);

	/**
	 * 批量新增
	 * @param books
	 * @return
	 */
	Integer insertBookBatch(List<Book> books);

    List<Clazz> selectBooks();
}
