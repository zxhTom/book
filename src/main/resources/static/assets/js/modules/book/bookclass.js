$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'book/bookClass/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'bookClassId', name: 'bookClassId', index: 'BOOK_CLASS_ID', width: 50, key: true },
			{ label: '书籍类别', name: 'className', index: 'CLASS_NAME', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		bookClass: {},
		bookClassModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.bookClassModalId = true;
			vm.title = "新增";
			vm.bookClass = {};
		},
		update: function (event) {
			var bookClassId = getSelectedRow();
			if(bookClassId == null){
				return ;
			}
            vm.bookClassModalId = true;
            vm.title = "修改";
            
            vm.getInfo(bookClassId)
		},
		saveOrUpdate: function (event) {
			var url = vm.bookClass.bookClassId == null ? "book/bookClass/insert" : "book/bookClass/update";
			var ajaxType = vm.bookClass.bookClassId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.bookClass),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.bookClassModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var bookClassIds = getSelectedRows();
			if(bookClassIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "book/bookClass/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(bookClassIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(bookClassId){
			$.get(baseURL + "book/bookClass/"+bookClassId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.bookClass = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});