package com.zxhtom.book.repository.impl;

import java.util.List;

import com.zxhtom.book.model.Clazz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import com.github.pagehelper.PageHelper;
import com.zxhtom.PagedResult;
import com.zxhtom.book.mapper.BookMapper;
import com.zxhtom.book.model.Book;
import com.zxhtom.book.repository.BookRepository;

import tk.mybatis.mapper.entity.Example;

@Repository
public class BookRepositoryImpl implements BookRepository{

	@Autowired
	private BookMapper bookMapper;
	@Override
	public PagedResult<Book> selectBooksByPK(Long bookId,Integer pageNumber, Integer pageSize) {
		PageHelper.startPage(pageNumber, pageSize);
		Example example = new Example(Book.class,false,false);
		example.createCriteria().andEqualTo("bookId", bookId);
		return new PagedResult<>(bookMapper.selectByExample(example));
	}
	@Override
	public Integer deleteBooksByPK(Long bookId) {
		Example example = new Example(Book.class,false,false);
		example.createCriteria().andEqualTo("bookId", bookId);
		return bookMapper.deleteByExample(example);
	}
	
	@Override
	public Integer updateBook(Book book) {
		return bookMapper.updateByPrimaryKeySelective(book);
	}
	@Override
	public Integer insertBook(Book book) {
		return bookMapper.insert(book);
	}
	
	@Override
	public Integer updateBookBatch(List<Book> books) {
		return bookMapper.updateBookBatch(books);
	}
	@Override
	public Integer insertBookBatch(List<Book> books) {
		return bookMapper.insertBookBatch(books);
	}

    @Override
    public List<Clazz> selectBooks() {
        return bookMapper.selectBooks();
    }

    @Override
    public List<Book> selectAll() {
        return bookMapper.selectAll();
    }
}
