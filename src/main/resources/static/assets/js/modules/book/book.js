$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'book/book/0',
        datatype: "json",
        beforeSend : function(request) {
				request.setRequestHeader('menuId', 'application/json');
		},
        colModel: [			
			{ label: 'bookId', name: 'bookId', index: 'BOOK_ID', width: 50, key: true },
			{ label: '类别id', name: 'bookClassId', index: 'BOOK_CLASS_ID', width: 80 }, 			
			{ label: '书籍名称', name: 'bookName', index: 'BOOK_NAME', width: 80 }, 			
			{ label: '书籍链接', name: 'bookUrl', index: 'BOOK_URL', width: 80 },
            { label: '上传者', name: 'userName', index: 'USER_NAME', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "data.datas",
            page: "data.pageNumber",
            total: "data.totalPages",
            records: "data.totalRecords"
        },
        prmNames : {
            page:"pageNumber", 
            rows:"pageSize", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		book: {},
		bookModalId:false,
		loading:true
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.bookModalId = true;
			vm.title = "新增";
			vm.book = {};
		},
		update: function (event) {
			var bookId = getSelectedRow();
			if(bookId == null){
				return ;
			}
            vm.bookModalId = true;
            vm.title = "修改";
            
            vm.getInfo(bookId)
		},
		saveOrUpdate: function (event) {
			var url = vm.book.bookId == null ? "book/book/insert" : "book/book/update";
			var ajaxType = vm.book.bookId == null ? "POST" : "PUT";
			$.ajax({
				type: ajaxType,
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.book),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
                            vm.bookModalId = false;
						});
					}else{
						alert(r.message);
                        vm.loading = false;
                        setTimeout(() =>{
                            vm.$nextTick(() => {
                            vm.loading = true
                    })},20);
					}
				}
			});
		},
		del: function (event) {
			var bookIds = getSelectedRows();
			if(bookIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "DELETE",
				    url: baseURL + "book/book/deleteBatch",
                    contentType: "application/json",
				    data: JSON.stringify(bookIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(bookId){
			$.get(baseURL + "book/book/"+bookId,{"pageNumber":1,  "pageSize":1}, function(r){
                vm.book = r.data.datas[0];
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});