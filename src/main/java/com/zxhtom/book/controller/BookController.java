package com.zxhtom.book.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.zxhtom.book.config.TomcatConfig;
import com.zxhtom.book.config.model.BookConfig;
import com.zxhtom.book.model.Clazz;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresRoles;
import com.zxhtom.vconstant.RoleList;
import com.zxhtom.PagedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zxhtom.book.model.Book;
import com.zxhtom.book.service.BookService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/book/book")
@Api(tags = "book book", description = "书籍信息表管理")
public class BookController {

	@Autowired
	private BookConfig bookConfig;
	@Autowired
	private TomcatConfig tomcatConfig;
	@Autowired
	private BookService bookService;
	/***
	 *
	 * 日志工具
	 */
	public Logger logger = LogManager.getLogger(BookController.class);

	@RequestMapping(value = "/clazzList" , method = RequestMethod.GET)
	@ApiOperation(value = "书籍列表")
	public List<Clazz> clazzList() {
		List<Clazz> clazzList = new ArrayList<>();
		List<Book> bookList = new ArrayList<>();
		Book book = new Book();
		book.setBookName("射雕英雄传");
		book.setBookUrl("http://47.98.129.1/test.pdf");
		setBookUrl(book);
		bookList.add(book);
		Clazz clazz = new Clazz();
		clazz.setClassName("武侠");
		clazz.setBookList(bookList);
		clazzList.add(clazz);
		//return clazzList;
        return bookService.selectBooksList();
	}

	private void setBookUrl(Book book) {
		book.setBookUrl("viewer.html?file=http://localhost:"+tomcatConfig.getPort()+"/source/"+book.getBookUrl());

	}

	@RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
	@ApiOperation(value = "根据主键获取所有书籍信息列表 传0表示查询所有")
	public PagedResult<Book> selectBooksByPK(@ApiParam(value = "书籍主键", required = true) @PathVariable @NotNull Long bookId,
	@ApiParam(value = "页码", required = false) @RequestParam(required = false) Integer pageNumber,
	@ApiParam(value = "页量", required = false) @RequestParam(required = false) Integer pageSize) {
		return bookService.selectBooksByPK(bookId, pageNumber,  pageSize);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	@ApiOperation(value = "单条删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteBookByPK(@ApiParam(value = "主键", required = true) @RequestParam @NotNull Long bookId) {
		return bookService.deleteBookPK(bookId);
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ApiOperation(value = "单条更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer update(@ApiParam(value = "通过主键更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) Book book) {
		return bookService.updateBook(book);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "单条新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insert(@ApiParam(value="新增数据")@RequestBody Book book) {
		return bookService.insertBook(book);
	}
	
	@RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
	@ApiOperation(value = "批量删除")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer deleteBookByPKBatch(@ApiParam(value = "主键集合", required = true) @RequestBody @NotNull List<Long> bookIds) {
		return bookService.deleteBookPKBatch(bookIds);
	}
	
	@RequestMapping(value = "/updateBatch", method = RequestMethod.PUT)
	@ApiOperation(value = "批量更新")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer updateBatch(@ApiParam(value = "通过学生id更新学生信息,不填的字段不更新", required = true) @RequestBody(required = true) List<Book> books) {
		return bookService.updateBookBatch(books);
	}

	@RequestMapping(value = "/insertBatch", method = RequestMethod.POST)
	@ApiOperation(value = "批量新增")
	@RequiresRoles(value={RoleList.SUPERADMIN})
	public Integer insertBatch(@ApiParam(value="新增数据")@RequestBody List<Book> books) {
		return bookService.insertBookBatch(books);
	}
}
