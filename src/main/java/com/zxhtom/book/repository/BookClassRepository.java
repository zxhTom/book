package com.zxhtom.book.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.book.model.BookClass;

public interface BookClassRepository {

	/**
	 * 根据主键查询书籍类别信息
	 * 
	 * @param bookClassId
	 *            书籍类别主键
	 * @return
	 */
	PagedResult<BookClass> selectBookClasssByPK(Long bookClassId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除书籍类别信息
	 * @param bookClassId
	 * @return
	 */
	Integer deleteBookClasssByPK(Long bookClassId);
	
	/**
	 * 单条更新书籍类别信息
	 * @param bookClasss
	 * @return
	 */
	Integer updateBookClass(BookClass bookClass);

	/**
	 * 单条新增信息
	 * @param bookClasss
	 * @return
	 */
	Integer insertBookClass(BookClass bookClass);
	
	/**
	 * 批量更新书籍类别信息
	 * @param bookClasss
	 * @return
	 */
	Integer updateBookClassBatch(List<BookClass> bookClasss);

	/**
	 * 批量新增信息
	 * @param bookClasss
	 * @return
	 */
	Integer insertBookClassBatch(List<BookClass> bookClasss);

    List<BookClass> selectAll();
}
