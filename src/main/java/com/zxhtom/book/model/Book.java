package com.zxhtom.book.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;

@ApiModel(description = "书籍表")
@Table(name = "BOOK")
public class Book {

	@ApiModelProperty(value = "用户Id")
	private String UserName;
	/**
	 * BOOK_ID
	 */
	@ApiModelProperty(value = "BOOK_ID")
	@Id
	private BigDecimal bookId;
	/**
	 * 类别id
	 */
	@ApiModelProperty(value = "类别id")
	private BigDecimal bookClassId;
	/**
	 * 书籍名称
	 */
	@ApiModelProperty(value = "书籍名称")
	private String bookName;
	/**
	 * 书籍链接
	 */
	@ApiModelProperty(value = "书籍链接")
	private String bookUrl;

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	/**
	 * 设置：BOOK_ID
	 */
	public void setBookId(BigDecimal bookId) {
		this.bookId = bookId;
	}
	/**
	 * 获取：BOOK_ID
	 */
	public BigDecimal getBookId() {
		return bookId;
	}
	/**
	 * 设置：类别id
	 */
	public void setBookClassId(BigDecimal bookClassId) {
		this.bookClassId = bookClassId;
	}
	/**
	 * 获取：类别id
	 */
	public BigDecimal getBookClassId() {
		return bookClassId;
	}
	/**
	 * 设置：书籍名称
	 */
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	/**
	 * 获取：书籍名称
	 */
	public String getBookName() {
		return bookName;
	}
	/**
	 * 设置：书籍链接
	 */
	public void setBookUrl(String bookUrl) {
		this.bookUrl = bookUrl;
	}
	/**
	 * 获取：书籍链接
	 */
	public String getBookUrl() {
		return bookUrl;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId+",bookClassId=" + bookClassId + ",bookName=" + bookName + ",bookUrl=" + bookUrl + "]";
	}

}
