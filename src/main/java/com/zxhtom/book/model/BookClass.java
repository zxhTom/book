package com.zxhtom.book.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;

@ApiModel(description = "书籍类别表")
@Table(name = "BOOK_CLASS")
public class BookClass {

	/**
	 * 类别id
	 */
	@ApiModelProperty(value = "类别id")
	@Id
	private BigDecimal bookClassId;
	/**
	 * 书籍类别
	 */
	@ApiModelProperty(value = "书籍类别")
	private String className;
	
	/**
	 * 设置：类别id
	 */
	public void setBookClassId(BigDecimal bookClassId) {
		this.bookClassId = bookClassId;
	}
	/**
	 * 获取：类别id
	 */
	public BigDecimal getBookClassId() {
		return bookClassId;
	}
	/**
	 * 设置：书籍类别
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * 获取：书籍类别
	 */
	public String getClassName() {
		return className;
	}

	@Override
	public String toString() {
		return "BookClass [bookClassId=" + bookClassId+",className=" + className + "]";
	}

}
