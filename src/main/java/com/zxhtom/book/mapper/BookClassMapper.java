package com.zxhtom.book.mapper;


import java.util.List;

import com.zxhtom.book.model.BookClass;

import tk.mybatis.mapper.common.Mapper;
public interface BookClassMapper extends Mapper<BookClass>{

	/**
	 * 批量更新
	 * @param bookClasss
	 * @return
	 */
	Integer updateBookClassBatch(List<BookClass> bookClasss);

	/**
	 * 批量新增
	 * @param bookClasss
	 * @return
	 */
	Integer insertBookClassBatch(List<BookClass> bookClasss);
}
