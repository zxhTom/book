package com.zxhtom.book.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.book.model.Book;
import com.zxhtom.book.model.Clazz;

public interface BookService {

	/**
	 * 根据主键查询书籍信息
	 * @param bookId 书籍主键
	 * @return
	 */
	PagedResult<Book> selectBooksByPK(Long bookId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除书籍信息
	 * @param bookId 书籍主键
	 * @return
	 */
	Integer deleteBookPK(Long bookId);

	/**
	 * 单条更新数据
	 * @param Book
	 * @return
	 */
	Integer updateBook(Book book);

	/**
	 * 单条新增数据
	 * @param book
	 * @return
	 */
	Integer insertBook(Book book);
	
	/**
	 * 批量根据主键删除书籍信息
	 * @param bookIds 书籍主键
	 * @return
	 */
	Integer deleteBookPKBatch(List<Long> bookIds);
	
	/**
	 * 批量更新数据
	 * @param books
	 * @return
	 */
	Integer updateBookBatch(List<Book> books);

	/**
	 * 批量新增数据
	 * @param Books
	 * @return
	 */
	Integer insertBookBatch(List<Book> books);

    List<Clazz> selectBooksList();
}
