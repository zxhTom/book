package com.zxhtom.book.service;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.book.model.BookClass;

public interface BookClassService {

	/**
	 * 根据主键查询书籍类别信息
	 * @param bookClassId 书籍类别主键
	 * @return
	 */
	PagedResult<BookClass> selectBookClasssByPK(Long bookClassId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除书籍类别信息
	 * @param bookClassId 书籍类别主键
	 * @return
	 */
	Integer deleteBookClassPK(Long bookClassId);

	/**
	 * 单条更新数据
	 * @param BookClass
	 * @return
	 */
	Integer updateBookClass(BookClass bookClass);

	/**
	 * 单条新增数据
	 * @param bookClass
	 * @return
	 */
	Integer insertBookClass(BookClass bookClass);
	
	/**
	 * 批量根据主键删除书籍类别信息
	 * @param bookClassIds 书籍类别主键
	 * @return
	 */
	Integer deleteBookClassPKBatch(List<Long> bookClassIds);
	
	/**
	 * 批量更新数据
	 * @param bookClasss
	 * @return
	 */
	Integer updateBookClassBatch(List<BookClass> bookClasss);

	/**
	 * 批量新增数据
	 * @param BookClasss
	 * @return
	 */
	Integer insertBookClassBatch(List<BookClass> bookClasss);

    List<BookClass> selectALL();
}
