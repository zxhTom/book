package com.zxhtom.book.repository;

import java.util.List;

import com.zxhtom.PagedResult;
import com.zxhtom.book.model.Book;
import com.zxhtom.book.model.Clazz;

public interface BookRepository {

	/**
	 * 根据主键查询书籍信息
	 * 
	 * @param bookId
	 *            书籍主键
	 * @return
	 */
	PagedResult<Book> selectBooksByPK(Long bookId,Integer pageNumber, Integer pageSize);

	/**
	 * 根据主键删除书籍信息
	 * @param bookId
	 * @return
	 */
	Integer deleteBooksByPK(Long bookId);
	
	/**
	 * 单条更新书籍信息
	 * @param books
	 * @return
	 */
	Integer updateBook(Book book);

	/**
	 * 单条新增信息
	 * @param books
	 * @return
	 */
	Integer insertBook(Book book);
	
	/**
	 * 批量更新书籍信息
	 * @param books
	 * @return
	 */
	Integer updateBookBatch(List<Book> books);

	/**
	 * 批量新增信息
	 * @param books
	 * @return
	 */
	Integer insertBookBatch(List<Book> books);

    List<Clazz> selectBooks();

    List<Book> selectAll();
}
